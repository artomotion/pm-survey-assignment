# Ball trajectory tracking survey

As a project manager, your supervisor assigned your team to develop a new feature which is analysing a bunch of pitch videos and extracting the ball trajectories for further research. However, you don't have any experience regarding this and the engineers in your team don't have it either. In order to make sure the things are on the track during the development, you might need to do your own research and guide your teammates.

In this survey, you need to figure out some potential solutions for this task and understand as more as possible for those technical details, so you will have abilities to present it to your teammates, assign tasks to proper people and coordinate the progress between dev team and your supervisor.

Here are some reminders, please keep in mind before your presentation.

- The input is a video and the output is a list of coordinates. Each coordinate represents the ball location on a certain frame.
- You don't need to write code, but you need to know the direction and what the mechanism is for the specific solution.
- You might have multiple solutions, and know what pros and cons for each solution.
- Quote everything if it's not your words.

##### Hint:
- Only few objects change in a sequence of frames, maybe you can do image diff to find the moving objects.
- AI is also a possible way to achieve.
